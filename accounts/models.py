# -*- coding: utf-8 -*-
# vim: set expandtab tabstop=4 shiftwidth=4:

from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

import hashlib
import random
import urllib
from reversion import revisions

from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils import six
from django.utils.encoding import smart_str
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.gis.geoip import GeoIP
from django.contrib.sessions.models import Session

from core import logging

from allauth.account.signals import user_signed_up
from allauth.account.signals import user_logged_in
from django.dispatch import receiver

__all__ = ('User',)


class UserManager(BaseUserManager):
    """Custom user manager required for things like createsuperuser command."""

    def create_user(self, email, password=None, **extra_fields):
        """Creates and saves a User with the given email and password."""
        now = timezone.now()
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=UserManager.normalize_email(email),
            last_login=now,
            date_joined=now,
            **extra_fields
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        """Creates and saves a superuser with the given email and password."""
        user = self.create_user(email, password=password, **extra_fields)
        user.is_active = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


@revisions.register
class User(AbstractBaseUser, PermissionsMixin):
    """Stores generic user information.

    This model contains information required to be recorded about company and
    individual users.

    """

    email = models.EmailField(
        'email address',
        max_length=255,
        unique=True,
        db_index=True)

    activation_key = models.CharField(
        _('activation key'),
        max_length=40,
        null=True,
        unique=True,
        blank=True,
        default=None)

    login_attempt = models.IntegerField(
        default=0,
        null=False)

    is_active = models.BooleanField(
        default=False)

    is_staff = models.BooleanField(
        default=False)

    date_joined = models.DateTimeField(
        'Date joined',
        help_text=u'Date and time when the user registered',
        auto_now_add=True)

    last_update = models.DateTimeField(
        'Last update',
        help_text=u'Date and time of the last modification of the user',
        auto_now=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = [],

    class Meta:
        app_label = 'accounts'

