
# Create your models here.
# -*- coding: utf-8 -*-
# vim: set expandtab tabstop=4 shiftwidth=4:
from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

import operator

from django.db import models
from django.db.models import Q
from django.db.models import QuerySet
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from mptt.models import MPTTModel
from mptt.models import TreeForeignKey
from mptt.managers import TreeManager



__all__ = (
    'Comment',
    'CommentContext',
    'LogLevel',
    'Log',
    'LogContext',
)


class CommentQuerySet(models.QuerySet):
    """Custom queryset for Comment model that works similar to fsm state log."""

    def _get_content_type(self, obj):
        return ContentType.objects.get_for_model(obj)

    def for_(self, *args):
        """Queryset all log messages for given object."""
        filters = []
        is_public = True
        for obj in args:
            if isinstance(obj, QuerySet):
                for o in obj:
                    if type(obj.pk) is int:
                        filters.append(
                            Q(content_type=self._get_content_type(o),
                              object_id=o.pk))
            elif type(obj) is bool:
                is_public = obj
            elif type(obj.pk) is int:
                filters.append(
                    Q(content_type=self._get_content_type(obj),
                      object_id=obj.pk))

        if len(filters) > 0:
            pks = CommentContext.objects \
                .filter(reduce(operator.or_, filters)) \
                .values_list('comment__pk', flat=True)
            return self.filter(pk__in=pks, is_public=is_public).order_by('pk')
        return self.none()


class CommentManager(models.Manager.from_queryset(CommentQuerySet),
                     TreeManager):
    """Empty class just to get the correct inheritance."""

    pass


@python_2_unicode_compatible
class Comment(MPTTModel):
    """Generic model to store comments."""

    comment = models.TextField()

    by = models.ForeignKey(
        'accounts.User')

    date_created = models.DateTimeField(
        auto_now_add=True)

    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='children',
        db_index=True)

    is_public = models.BooleanField(
        default=True,
        help_text='Only staff members can view private comments')

    objects = CommentManager()

    def __str__(self):
        return '{} commented {}'.format(self.by, self.comment)

    def add_context(self, obj):
        """Add object context to the comment."""
        ct = ContentType.objects.get_for_model(obj)
        return CommentContext.objects.create(
            comment=self,
            content_type=ct,
            object_id=obj.pk)

    def get_context(self, content_object_type=None):
        """Get context objects for comment."""
        objs = list()
        for row in CommentContext.objects.filter(comment=self):
            if (content_object_type is not None and \
                        isinstance(row.content_object, content_object_type)) or \
                            content_object_type is None:
                objs.append(row.content_object)
        return objs


class CommentContext(models.Model):
    """Give comments context by assigning to other model objects.

    This uses generic foreign keys.

    """

    comment = models.ForeignKey(
        Comment)

    content_type = models.ForeignKey(
        ContentType)

    object_id = models.PositiveIntegerField(
        db_index=True)

    content_object = GenericForeignKey(
        'content_type',
        'object_id')


class LogLevel(object):

    """Define log levels."""

    DEBUG = 'DEBUG'
    INFO = 'INFO'
    WARNING = 'WARNING'
    ERROR = 'ERROR'
    CRITICAL = 'CRITICAL'

    CHOICES = (
        (DEBUG, 'Debug'),
        (INFO, 'Info'),
        (WARNING, 'Warning'),
        (ERROR, 'Error'),
        (CRITICAL, 'Critical'),
    )


class LogQuerySet(models.QuerySet):

    """Custom queryset for Log model that works similar to fsm state log."""

    def _get_content_type(self, obj):
        return ContentType.objects.get_for_model(obj)

    def for_(self, *args):
        """Queryset all log messages for given object."""
        filters = []
        for obj in args:
            if isinstance(obj, QuerySet):
                for o in obj:
                    filters.append(Q(content_type=self._get_content_type(o),
                                     object_id=o.pk))
            else:
                filters.append(Q(content_type=self._get_content_type(obj),
                                 object_id=obj.pk))

        if len(filters) > 0:
            pks = LogContext.objects \
                .filter(reduce(operator.or_, filters)) \
                .values_list('log__pk', flat=True)
            return self.filter(pk__in=pks).order_by('pk')
        return self.none()


@python_2_unicode_compatible
class Log(models.Model):

    """Model for storing log messages."""

    level = models.CharField(
        max_length=10,
        choices=LogLevel.CHOICES)

    stamp = models.DateTimeField(
        verbose_name='Date Time Stamp',
        help_text='Date and time of the log record',
        auto_now_add=True)

    is_public = models.BooleanField(
        default=False,
        help_text='Can non staff members view this log message')

    message = models.TextField(
        help_text='Recorded message')

    by = models.ForeignKey(
        'accounts.User',
        null=True,
        blank=True,
        default=None)

    objects = LogQuerySet.as_manager()

    def __str__(self):
        return '%s - %s' % (self.stamp, self.message)

    def add_context(self, obj):
        """Add object context to the log entry."""
        ct = ContentType.objects.get_for_model(obj)
        return LogContext.objects.create(
            log=self,
            content_type=ct,
            object_id=obj.pk)

    def get_context(self, content_object_type=None):
        """Get context objects for Log."""
        objs = list()
        for row in LogContext.objects.filter(log=self):
            if (content_object_type is not None and \
                    isinstance(row.content_object, content_object_type)) or \
                    content_object_type is None:
                objs.append(row.content_object)
        return objs

    def get_comments(self):
        """Get a list of comments on this log message."""
        from .comments import Comment
        return Comment.objects.root_nodes().for_(self)


class LogContext(models.Model):

    """Give Log objects context by assigning to other model objects.

    This uses generic foreign keys.

    """

    log = models.ForeignKey(
        Log)

    content_type = models.ForeignKey(
        ContentType)

    object_id = models.PositiveIntegerField(
        db_index=True)

    content_object = GenericForeignKey(
        'content_type',
        'object_id')
